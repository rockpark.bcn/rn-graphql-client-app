/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {ApolloProvider, Query} from "react-apollo";
import { GET_USERS } from "./queries";
import client from "./client";

type Props = {};
export default class App extends Component<Props> {
  render() {
    return (
        <ApolloProvider client={client}>

            <View style={styles.container}>
                <Text style={[styles.welcome, {color: "#333"}]}>Users</Text>

                <Query query={GET_USERS}>
                    {
                        ({loading, data, error}) => {
                            if (loading) return <Text>loading...</Text>;
                            if (error) return <Text>something's wrong...</Text>
                            return data.users.map(user => (
                                <View key={ user.id } style={styles.buttonStyle}>
                                    <Text style={{textAlign: "center"}}>{ user.name } &lt;{user.email}&gt;</Text>
                                </View>
                            ));
                        }
                    }
                </Query>
            </View>
        </ApolloProvider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
    buttonStyle: {padding: 10, width: 250, borderRadius: 5, borderWidth: 0.5, margin: 3, backgroundColor: "#ddddee"}
});
